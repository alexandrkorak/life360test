package solution.manager

import solution.data.Circle
import solution.data.Member

class DefaultCircleManager : CircleManager {
    private val circleIdCircle = HashMap<Int, Circle>()
    private val sizeCircleCount = HashMap<Int, Int>()

    @Synchronized
    override fun createCircle(circleId: Int, member: Member): Circle =
        circleIdCircle[circleId] ?: Circle(id = circleId, member = member).apply {
            circleIdCircle[circleId] = this
            sizeCircleCount[1] = sizeCircleCount[1]?.inc() ?: 1
        }

    @Synchronized
    override fun addMember(circleId: Int, member: Member): Circle {
        val prevCircle: Circle? = circleIdCircle[circleId]
        return if (prevCircle != null) {
            val newCircle = prevCircle.addMember(member)
            circleIdCircle[circleId] = newCircle
            if (newCircle.size() > prevCircle.size()) {
                sizeCircleCount[newCircle.size()] = sizeCircleCount[newCircle.size()]?.inc() ?: 1
                sizeCircleCount[prevCircle.size()] = sizeCircleCount[prevCircle.size()]!!.dec()
            }
            newCircle
        } else {
            Circle(id = circleId, member = member).apply {
                circleIdCircle[circleId] = this
                sizeCircleCount[1] = sizeCircleCount[1]?.inc() ?: 1
            }
        }
    }

    @Synchronized
    override fun removeMember(circleId: Int, memberId: Int): Circle? {
        val prevCircle = circleIdCircle[circleId]
        return if (prevCircle != null) {
            val newCircle = prevCircle.removeMember(memberId)
            circleIdCircle[circleId] = newCircle
            if (newCircle.size() < prevCircle.size()) {
                sizeCircleCount[prevCircle.size()] = sizeCircleCount[prevCircle.size()]!!.dec()
                if (newCircle.size() >= 1) {
                    sizeCircleCount[newCircle.size()] = sizeCircleCount[newCircle.size()]?.inc() ?: 1
                } else {
                    circleIdCircle.remove(circleId)
                }
            }
            newCircle
        } else {
            null
        }
    }

    @Synchronized
    override fun query(memberCount: Int): Int = sizeCircleCount[memberCount] ?: 0
}