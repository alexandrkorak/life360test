package solution.manager

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import solution.data.Member
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull

internal class DefaultCircleManagerTest {

    private lateinit var circleManager: CircleManager

    @BeforeEach
    fun setUp() {
        circleManager = DefaultCircleManager()
    }

    @Test
    fun createCircle() {
        val memberId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        val member = Member(id = memberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())

        val circleId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        val circle = circleManager.createCircle(circleId, member)

        val anotherMemberId = memberId.dec()
        val anotherMember =
            Member(id = anotherMemberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())

        val anotherCircle = circleManager.createCircle(circleId, anotherMember)

        assertEquals(circle, anotherCircle)
    }

    @Test
    fun addMember() {
        val memberId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        val member = Member(id = memberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())

        val circleId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        val circle = circleManager.addMember(circleId, member)
        assertEquals(member, circle.getMember(memberId))
        assertEquals(1, circle.size())

        val anotherMemberId = memberId.dec()
        val anotherMember =
            Member(id = anotherMemberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())
        val anotherCircle = circleManager.addMember(circleId, anotherMember)

        assertNotEquals(circle, anotherCircle)
        assertEquals(circle.id, anotherCircle.id)
        assertEquals(circle.name, anotherCircle.name)
        assertEquals(anotherMember, anotherCircle.getMember(anotherMemberId))
        assertEquals(2, anotherCircle.size())

        val members = anotherCircle.getMembersWithin(Double.MAX_VALUE, 0.0, 0.0)
        return
    }

    @Test
    fun removeMember() {
        val memberId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        val circleId: Int = Random.nextInt(1, Integer.MAX_VALUE)

        assertNull(circleManager.removeMember(circleId, memberId))


        val member = Member(id = memberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())
        val circle = circleManager.addMember(circleId, member)

        assertEquals(member, circle.getMember(memberId))
        assertEquals(1, circle.size())

        val anotherMemberId = memberId.dec()
        val anotherMember =
            Member(id = anotherMemberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())
        val anotherCircle = circleManager.addMember(circleId, anotherMember)

        assertNotEquals(circle, anotherCircle)
        assertEquals(circle.id, anotherCircle.id)
        assertEquals(circle.name, anotherCircle.name)
        assertEquals(1, circle.size())
        assertEquals(2, anotherCircle.size())


        val circleWithRemovedMember = circleManager.removeMember(circleId, memberId)
        assertEquals(1, circleWithRemovedMember!!.size())
        assertNull(circleWithRemovedMember.getMember(memberId))
        assertEquals(anotherMember, circleWithRemovedMember.getMember(anotherMemberId))

        val circleWithRemovedAnotherMember = circleManager.removeMember(circleId, anotherMemberId)
        assertEquals(0, circleWithRemovedAnotherMember!!.size())
        assertNull(circleWithRemovedAnotherMember.getMember(anotherMemberId))
    }

    @Test
    fun query() {
        assertEquals(0, circleManager.query(1))

        val memberId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        val member = Member(id = memberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())

        val circleId: Int = Random.nextInt(1, Integer.MAX_VALUE)
        circleManager.createCircle(circleId, member)

        assertEquals(1, circleManager.query(1))

        val anotherMemberId = memberId.dec()
        val anotherMember =
            Member(id = anotherMemberId, longitude = Random.nextDouble(), latitude = Random.nextDouble())
        circleManager.addMember(circleId, anotherMember)

        assertEquals(0, circleManager.query(1))
        assertEquals(1, circleManager.query(2))

        circleManager.removeMember(circleId, memberId)
        assertEquals(1, circleManager.query(1))
        circleManager.removeMember(circleId, anotherMemberId)
        assertEquals(0, circleManager.query(1))
    }
}