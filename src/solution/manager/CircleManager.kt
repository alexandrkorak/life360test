package solution.manager

import solution.data.Circle
import solution.data.Member

/**
 * A Circle manager that can add and remove members based on a circle ID.
 * Also able to query the number of circles with a given number of parameters.
 */
interface CircleManager {
    /**
     * Creates a new circle with 1 member and the provided ID,
     * or return circle for specified ID if it's already exists.
     */
    fun createCircle(circleId: Int, member: Member): Circle

    /**
     * Adds a member to a circle of the id passed in to the signature.
     * Creates a circle if it does not exists.
     */
    fun addMember(circleId: Int, member: Member): Circle

    /**
     * Removes a member from the circle of the id passed in to the signature.
     * If after member removal circle is empty - it should be removed as well.
     */
    fun removeMember(circleId: Int, memberId: Int): Circle?

    /**
     * Return the number of circles with members of size {@code memberCount} passed into the function in constant time O(1).
     */
    fun query(memberCount: Int): Int
}