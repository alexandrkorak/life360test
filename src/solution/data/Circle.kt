package solution.data

import solution.utils.distanceInMeters

/**
 * Immutable (to be able to be used with collections) Circle class.
 */
class Circle private constructor(
    val id: Int,
    val name: String = "",
    private val memberIdMember: HashMap<Int, Member> = HashMap()
) {
    constructor(id: Int, name: String = "", member: Member) : this(id = id, name = name) {
        memberIdMember[member.id] = member
    }

    /**
     * If Circle doesn't contain member with related unique id - adds the member and returns it,
     * returns currently stored member otherwise.
     */
    fun addMember(member: Member): Circle = Circle(id, name, HashMap(memberIdMember).apply { this[member.id] = member })

    /**
     * Removes member by id, and returns it, if it's stored in the circle. Returns {@code null} otherwise.
     */
    fun removeMember(memberId: Int): Circle = Circle(id, name, HashMap(memberIdMember).apply { this.remove(memberId) })

    /**
     * Returns member by {code memberId} if such exists, null otherwise.
     */
    fun getMember(memberId: Int): Member? = memberIdMember[memberId]

    fun getMembersWithin(distanceInMeters: Double, lat: Double, lon: Double): Set<Member> {
        return memberIdMember.values.filter {
            distanceInMeters(lat, lon, it.latitude, it.longitude).compareTo(
                distanceInMeters
            ) <= 0
        }.toSet()
    }

    fun size(): Int = memberIdMember.size

    override fun hashCode(): Int = id

    override fun equals(other: Any?): Boolean = other is Circle
            && id == other.id
            && memberIdMember.values == other.memberIdMember.values

    override fun toString(): String {
        return "[${javaClass.simpleName}(id=$id, name=$name, members=${memberIdMember.values})]"
    }
}