package solution.data

data class Member(
    val id: Int, val name: String = "",
    val longitude: Double, val latitude: Double
)
