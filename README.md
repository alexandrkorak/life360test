# README #

Repository provides simple solution for CircleManager in accordance to the test task requirements.
Solution is represented by the classes to be used.

To make data classes safe to be used with Collections and in multi-threaded environment:
they were made immutable.

### How to check it? ###

* DefaultCircleManagerTest provides ability to test the solution.

### How do I get set up? ###

* Import the project using Idea IDE.
* Run DefaultCircleManagerTest class.

### How solution may be improved ###

* Circles and members ID generation and distribution should be encapsulated from external API users.
* Circles and members name modification ability should be provided over CircleManager API.
* API for retrieving amount of Circles by members amount may be extended to return specific Circles or specific Circles IDs.